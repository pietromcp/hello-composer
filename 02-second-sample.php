<?php

require __DIR__ . '/vendor/autoload.php';
require('pietromcp/EhimeHelloWorldWrapper.php');

use pietromcp\EhimeHelloWorldWrapper;

$hello = new EhimeHelloWorldWrapper();
echo "\n--------------------\n" . $hello->sayHello() . "\n--------------------\n";
