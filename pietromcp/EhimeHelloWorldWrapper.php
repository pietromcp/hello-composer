<?php

namespace pietromcp;
use HelloWorld\SayHello;

class EhimeHelloWorldWrapper {
    private $hello;

    function __construct() {
        $this->hello = new SayHello();
    }

    function sayHello(): string {
        return "[sayHello] Hello, World! " . $this->hello->world();
    }
}